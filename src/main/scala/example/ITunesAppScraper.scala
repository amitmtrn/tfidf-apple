package example

import akka.http.scaladsl.Http
import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import scala.concurrent.ExecutionContextExecutor
import akka.http.scaladsl.model.HttpRequest
import scala.concurrent.Await
import scala.concurrent.Future
import akka.http.scaladsl.model.HttpResponse
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.unmarshalling.Unmarshal
import scala.concurrent.duration._
import spray.json._
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import DefaultJsonProtocol._ 
import akka.http.scaladsl.model.ResponseEntity


object ITunesAppScraper {
  implicit val system: ActorSystem = ActorSystem()
  implicit val materializer = ActorMaterializer()
  implicit val executionContext: ExecutionContextExecutor = system.dispatcher

  final case class Item(ipadScreenshotUrls: List[String], screenshotUrls: List[String], supportedDevices: List[String], releaseNotes: String, description: String)
  final case class AppleResponse(resultCount: Int, results: List[Item])

  implicit val itemFormat = jsonFormat5(Item)
  implicit val resultFormat = jsonFormat2(AppleResponse)

  def requestPage(id: String) = Http().singleRequest(HttpRequest(uri = s"http://itunes.apple.com/lookup?id=$id"));
  def parseJSON(entity: ResponseEntity) = {
    val json = Await.result(Unmarshal(entity).to[String], 1.second)
    try {
      Await.result(Unmarshal(json).to[AppleResponse], 1.second)
    } catch {
      case e: Throwable => new AppleResponse(0, List[Item]())
    }
  }
  def parsePage(response: Future[HttpResponse]): AppleResponse = Await.result(response.map {
    case HttpResponse(StatusCodes.OK, headers, entity, _) => parseJSON(entity)
    case err => throw new Exception(s"Error: $err")
  }, 60.second)
  def removeEmpty(response: AppleResponse) = response.results.length > 0
  def wordsCount(text: String):Map[String, Int] = 
    text
      // 160.toChar fix weird space character in 553834731 that did not catch on \s
      .split(s"[\\s${160.toChar}]+")
      .map(word => word.toLowerCase().replaceAll("[●•\\*\\~,!\\?\\.\"”:;\\(\\)-]", ""))
      .filter(word => word.length() != 0)
      .map(word => (word, 1)).groupBy(word => word._1)
      .map(word => (word._1, word._2.length))
  def countDescriptionWords(response: AppleResponse) = wordsCount(response.results(0).description)

  def scape(ids: Iterator[String]): Iterator[(String, Map[String, Int])] =
    ids
      .map(id => (id, requestPage(id)))
      .map(response => (response._1, parsePage(response._2)))
      .filter(page => removeEmpty(page._2))
      .map(page => (page._1, countDescriptionWords(page._2)))
  
  def done = {
    Http().shutdownAllConnectionPools()
    system.terminate()
  }

}