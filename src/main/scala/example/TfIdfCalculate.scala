package example

object TfIdfCalculate {
  def buildWordMap(pages: Iterator[(String, Map[String,Int])]) = {
    val (totalPages, wordsList) = pages.map(item => (1, item._2.keySet.toList)).reduce((a1, a2) => (a1._1 + a2._1, a1._2 ++ a2._2))
    val wordsMap = wordsList
      .map(word => (word, 1)).groupBy(word => word._1)
      .map(word => (word._1, word._2.length))
    (totalPages, wordsMap)
  }
  def tfCalculation(word: Float): Double = Math.log(1 + word)
  def idfCalculation(totalPages: Double, wordPagesCount: Double) = Math.log(1 + totalPages/wordPagesCount)
  def tfIdfCalculation(wordCount: Int, totalPages: Double, wordPagesCount: Double) = tfCalculation(wordCount) * idfCalculation(totalPages, wordPagesCount)

  def calculate(pages: Iterator[(String, Map[String,Int])], totalPages: Int, wordsMap: Map[String, Int]) = {
    pages.map(page => (
        page._1,
        page._2
          .map(word => (word._1, tfIdfCalculation(word._2, totalPages, wordsMap.getOrElse(word._1, 1).toDouble)))
          .toSeq.sortBy(_._2).reverse.take(10)
        )
      )
  }
}