package example

import scala.io.Source
import akka.actor.ActorSystem
import scala.concurrent.ExecutionContextExecutor
import akka.http.scaladsl.model.HttpRequest
import akka.http.scaladsl.Http
import scala.concurrent.Await
import akka.http.scaladsl.model.HttpResponse
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.ActorMaterializer
import akka.http.scaladsl.model.ResponseEntity
import scala.concurrent.Future

object Main {
  def main(args: Array[String]) {
    val applePages = ITunesAppScraper.scape(Source.fromFile(args(0)).getLines).duplicate
    val (totalPages, wordsMap) = TfIdfCalculate.buildWordMap(applePages._1)
    val tfIdf = TfIdfCalculate.calculate(applePages._2, totalPages, wordsMap)

    tfIdf.foreach(idWords => {
      val wordsRank = idWords._2.map(wordRank => f"${wordRank._1} ${wordRank._2}%.3f")
        .reduce((wordRank1, wordRank2) => s"$wordRank1, $wordRank2")

      println(s"${idWords._1}: $wordsRank")
    })

    ITunesAppScraper.done
  }
}
